{ sources = [ "./src/**/*.purs" ]
, name = "deku-starter"
, dependencies =
  [ "arrays"
  , "concur-core"
  , "concur-react"
  , "deku"
  , "effect"
  , "foldable-traversable"
  , "hyrule"
  , "prelude"
  , "tuples"
  , "web-html"
  ]
, packages = ./packages.dhall
}
