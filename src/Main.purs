module Main where

import Prelude

import Concur.Core (Widget)
import Concur.Core.FRP (Signal, dyn, hold, loopS)
import Concur.React (HTML)
import Concur.React.DOM as D
import Concur.React.Props as P
import Concur.React.Run (runWidgetInDom)
import Data.Array (replicate)
import Data.FunctorWithIndex (class FunctorWithIndex, mapWithIndex)
import Data.Maybe (Maybe(..))
import Effect (Effect)

main :: Effect Unit
main = runWidgetInDom "root" golWidget

newtype Grid a = Grid (Array (Array a))

type Coord = {row :: Int, col :: Int}

instance Functor Grid where
    map f (Grid g) = Grid $ map (map f) g
instance FunctorWithIndex Coord Grid where
    mapWithIndex f (Grid g) = Grid $ mapWithIndex (\row -> mapWithIndex (\col -> f {row, col})) g

modifyAt :: forall a. Coord -> (a -> a) -> Grid a -> Grid a
modifyAt {row, col} f = mapWithIndex (\{row:row', col:col'} -> if row' == row && col' == col then f else identity)

data Cell = Dead | Alive

gridOf :: forall a. (Coord -> a) -> Int -> Int -> Grid a
gridOf f numberOfRow numberOfCol = Grid $ mapWithIndex (\row -> mapWithIndex (\col _ -> f {row, col})) $ replicate numberOfRow (replicate numberOfCol unit)

initialCell :: Grid Cell
initialCell = gridOf (\_ -> Dead) 50 50

gridOfCell :: Grid Cell -> Signal HTML (Maybe Coord)
gridOfCell (Grid cell) = hold Nothing $ D.div [P.className "row"] $
    cell <#!> \row rows -> D.div [P.className "col"] $
        rows <#!> \col cell' ->
        let class' = case cell' of
                Alive -> "alive"
                Dead -> "dead"
        in
            D.button [Just {row, col} <$ P.onClick, P.className class'] []

flippedMapWithIndex :: forall i f a b. FunctorWithIndex i f => f a -> (i -> a -> b) -> f b
flippedMapWithIndex = flip mapWithIndex

infix 5 flippedMapWithIndex as <#!>

golWidget :: forall a. Widget HTML a
golWidget = dyn $ loopS initialCell \cells -> do
    clicked <- gridOfCell cells
    case clicked of
        Nothing -> pure cells
        Just coord -> pure $ modifyAt coord flipState cells

flipState :: Cell -> Cell
flipState Alive = Dead
flipState Dead = Alive